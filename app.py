from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import urllib as ul

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///messages.db'
db = SQLAlchemy(app)


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(500), nullable=False)
    body = db.Column(db.String(5000), nullable=False)
    footer = db.Column(db.String(500), nullable=False)
    subfooter = db.Column(db.String(500), nullable=False)
    image_url = db.Column(db.String(500), nullable=True)
    ratio = db.Column(db.String(500), nullable=False)
    theme = db.Column(db.String(500), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "<Message(id='%s', title='%s', body='%s', footer='%s', subfooter='%s', image_url='%s', ratio='%s', theme='%s')>" % (
            self.id, self.body, self.footer, self.subfooter, self.image_url, self.ratio, self.theme)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        footer = request.form['footer']
        subfooter = request.form['subfooter']
        image_url = request.form['image_url']
        ratio = request.form['ratio']
        theme = request.form['theme']

        params = {'t': title, 'b': body, 'f': footer, 'st': subfooter, 'img': image_url, 'rat': ratio, 'col': theme}
        url = "https://www.hlidacstatu.cz/socialbanner/quote?" + ul.parse.urlencode(params, doseq=True)

        try:
            messages = Message.query.order_by(Message.date_created.asc()).all()
            return render_template('index.html', title=title, body=body, footer=footer, subfooter=subfooter,
                                   image_url=image_url, url=url, messages=messages)
        except:
            return 'Template index.html was could not be rendered'
    else:
        try:
            messages = Message.query.all()
            return render_template('index.html', messages=messages)
        except:
            return render_template('index.html')


@app.route('/message/<int:id>')
def insert_message(id):
    msg = Message.query.get_or_404(id)
    title = msg.title
    footer = msg.footer
    body = msg.body
    subfooter = msg.subfooter
    image_url = msg.image_url
    theme = msg.theme

    try:
        messages = Message.query.order_by(Message.date_created.asc()).all()
        return render_template('index.html', title=title, body=body, footer=footer, subfooter=subfooter,
                               image_url=image_url, theme=theme, messages=messages)
    except:
        return 'Template index.html was could not be rendered'


@app.route('/delete/<int:id>')
def delete(id):
    message_to_delete = Message.query.get_or_404(id)

    try:
        db.session.delete(message_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'There was a problem deleting that task'


@app.route('/store/', methods=['GET', 'POST'])
def store():
    title = request.form.get('title')
    footer = request.form.get('footer')
    body = request.form.get("body")
    subfooter = request.form.get('subfooter')
    image_url = request.form.get('image_url')
    ratio = request.form.get('ratio')
    theme = request.form.get('theme')

    try:
        record = Message(title=title, body=body, footer=footer, subfooter=subfooter, image_url=image_url, ratio=ratio,
                         theme=theme)
        db.session.add(record)
        db.session.commit()

        return redirect('/')


    except:
        return 'There was an issue adding your task'


@app.route('/insert/<int:id>', methods=['GET', 'POST'])
def insert(id):
    message = Message.query.get_or_404(id)
    return render_template('update.html', message=message)


if __name__ == "__main__":
    app.run()
